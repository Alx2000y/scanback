/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* API for object-oriented usage of scanback compression                    */
/****************************************************************************/
#ifndef __SBAPI_H__
#define __SBAPI_H__

#include <stdint.h>
#include "../common/common_utilities.h"
#include "rlesbc.h"

extern "C"
{
#include "huffman.h"
}

namespace Componentality
{
	namespace Common
	{

		// Compressor interface (compresses blob to blob)
		class ICompressor
		{
		public:
			ICompressor() {};
			virtual ~ICompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob raw) = 0;
		};

		// Decompressor interface
		class IDecompressor
		{
		public:
			IDecompressor() {};
			virtual ~IDecompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob comressed) = 0;
		};

		// Sequential decompressor
		class ISerialDecompressor
		{
		public:
			ISerialDecompressor(const unsigned char* source) {};
			virtual ~ISerialDecompressor() {};
		public:
			virtual unsigned char operator()() = 0;
		};

		// RLE compressor
		class RLECompressor : public ICompressor
		{
		public:
			RLECompressor() {};
			virtual ~RLECompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob raw)
			{
				CST::Common::blob result(raw.mSize * 2 + 4);
				RLE_Encode((unsigned char*) raw.mData, raw.mSize, (unsigned char*) result.mData, result.mSize);
				return result;
			};
		};

		// RLE decompressor
		class RLEDecompressor : public IDecompressor
		{
		protected:
			size_t mDecodedSize;
		protected:
			RLEDecompressor() {};
		public:
			RLEDecompressor(const size_t raw_size) : mDecodedSize(raw_size) {};
			virtual ~RLEDecompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob compressed)
			{
				CST::Common::blob result(mDecodedSize);
				RLE_Decode((unsigned char*)compressed.mData, compressed.mSize, (unsigned char*)result.mData, result.mSize);
				return result;
			}
		};

		// RLE sequential decompressor
		class RLESerialDecompressor : public RLE_DECODE, public ISerialDecompressor
		{
		public:
			RLESerialDecompressor(const unsigned char* source) : ISerialDecompressor(source) { RLE_InitDecoder(this, (unsigned char*) source); }
			virtual ~RLESerialDecompressor() {};
		public:
			virtual unsigned char operator()()
			{
				return RLE_Fetch(this);
			}
		};

		// Scanback compressor
		class SBCompressor : public ICompressor
		{
		public:
			SBCompressor() {};
			virtual ~SBCompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob raw)
			{
				CST::Common::blob result(raw.mSize * 2 + 40);
				size_t bits = SB_Compress((unsigned char*) result.mData, (unsigned char*) raw.mData, raw.mSize);
				result.mSize = (bits + 7) / 8;
				return result;
			}
		};

		// Scanback decompressor
		class SBDecompressor : public IDecompressor
		{
		protected:
			size_t mRawSize;
		protected:
			SBDecompressor() {};
		public:
			SBDecompressor(const size_t raw_size) : mRawSize(raw_size) {};
			virtual ~SBDecompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob compressed)
			{
				CST::Common::blob result(mRawSize);
				SB_DECODER decoder;
				SB_InitDecoder(&decoder, (unsigned char*) compressed.mData);
				for (size_t i = 0; i < mRawSize; i++)
					result.mData[i] = (char) SB_Fetch(&decoder);
				return result;
			}
		};

		// Scanback serial decompressor
		class SBSerialDecompressor : public SB_DECODER, public ISerialDecompressor
		{
		public:
			SBSerialDecompressor(const unsigned char* compressed) : ISerialDecompressor(compressed) { SB_InitDecoder(this, (unsigned char*) compressed); }
			virtual ~SBSerialDecompressor() {};
		public:
			virtual unsigned char operator()() { return SB_Fetch(this); };
		};

		// RLE + Scanback compressor
		class RLESBCompressor : public ICompressor
		{
		protected:
			bool mForSerial;
		public:
			RLESBCompressor(const bool forserial = false) : mForSerial(forserial) {};
			virtual ~RLESBCompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob raw)
			{
				CST::Common::blob rle = RLECompressor()(raw);
				CST::Common::blob result = SBCompressor()(rle);
				CST::Common::blob size = !mForSerial ? CST::Common::numberToBlob<uint32_t>((uint32_t) rle.mSize) : CST::Common::blob();
				CST::Common::blob _result = size + result;
				rle.purge();
				result.purge();
				size.purge();
				return _result;
			}
		};

		// RLE + Scanback decompressor
		class RLESBDecompressor : public IDecompressor
		{
		protected:
			size_t mRawSize;
		protected:
			RLESBDecompressor() {};
		public:
			RLESBDecompressor(const size_t raw_size) : mRawSize(raw_size) {};
			virtual ~RLESBDecompressor() {};
		public:
			virtual CST::Common::blob operator()(const CST::Common::blob compressed)
			{
				if (!mRawSize) return CST::Common::blob();
				std::pair<CST::Common::blob, CST::Common::blob> size_and_compr = CST::Common::blob_split(compressed, sizeof(uint32_t));
				size_t size = CST::Common::blobToNumber<uint32_t>(size_and_compr.first);
				CST::Common::blob rle = SBDecompressor(size)(size_and_compr.second);
				size_and_compr.first.purge();
				size_and_compr.second.purge();
				CST::Common::blob result = RLEDecompressor(mRawSize)(rle);
				rle.purge();
				return result;
			}
		};

		// RLE + Scanback serial decompressor
		class RLESBSerialDecompressor : public ISerialDecompressor
		{
		protected:
			RLE_DECODE mRLE;
			SB_DECODER mSB;
			unsigned char mFetch;
		public:
			RLESBSerialDecompressor(const unsigned char* compressed) : ISerialDecompressor(compressed)
			{
				RLE_InitDecoder(&mRLE, NULL);
				SB_InitDecoder(&mSB, (unsigned char*) compressed);
				mFetch = 0;
			}
			virtual ~RLESBSerialDecompressor() {}
		public:
			virtual unsigned char operator()()
			{
				return RLESB_Fetch(&mRLE, &mSB, &mFetch);
			}
		};

		// Huffman compressor
		class HuffmanCompressor
		{
		protected:
			HuffmanDescriptor mDescriptor;
			HuffmanFrqTable mFrequencyTable;
		public:
			HuffmanCompressor() { huffmanFrqTableInit(&mFrequencyTable); huffmanGetDescriptor(&mFrequencyTable, &mDescriptor); };
			virtual ~HuffmanCompressor() {};
		public:
			virtual void updateModel(CST::Common::blob data)
			{
				huffmanFrqTableUpdate(&mFrequencyTable, data.mData, data.mSize);
				huffmanGetDescriptor(&mFrequencyTable, &mDescriptor);
			}
			virtual CST::Common::blob operator()(const CST::Common::blob raw) 
			{
				
			}
		};



	} // namespace Common
} // namespace Componentality

#endif
