/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* RLE compression implementation                                           */
/****************************************************************************/
#include "rle.h"
#include <memory.h>

using namespace Componentality::Common;

// Do 8-bit RLE encoding
void Componentality::Common::RLE_Encode(unsigned char* source, size_t source_size, unsigned char* target, size_t& target_size)
{
	target_size = 0;
	unsigned char previous_character = source[0];
	unsigned char series_counter = 1;
	bool same = false;
	size_t i;
	for (i = 1; i < source_size; i++)
	{
		// If current byte is equal to previous
		if (source[i] == previous_character)
		{
			// If we process sequence of the same characters
			if (same)
			{
				if (series_counter < 127)
					series_counter++;
				else
				{
					target[target_size++] = 0x80 | series_counter;
					target[target_size++] = previous_character;
					series_counter = 1;
				}
			}
			else
			{
				if (series_counter > 1)
				{
					target[target_size++] = series_counter - 1;
					memcpy(target + target_size, source + i - series_counter, series_counter - 1);
					target_size += series_counter - 1;
				}
				series_counter = 2;
				same = true;
			}
		}
		else
		{
			if (same)
			{
				if (series_counter > 1)
				{
					target[target_size++] = 0x80 | series_counter;
					target[target_size++] = previous_character;
					series_counter = 1;
				}
				else
					series_counter += 1;
				same = false;
			}
			else
			{
				if (series_counter == 127)
				{
					target[target_size++] = series_counter;
					memcpy(target + target_size, source + i - series_counter, series_counter);
					target_size += series_counter;
					series_counter = 1;
				}
				else
					series_counter++;
			}
		}
		previous_character = source[i];
	}
	if (same)
	{
		target[target_size++] = 0x80 | series_counter;
		target[target_size++] = previous_character;
	}
	else
	{
		target[target_size++] = series_counter;
		memcpy(target + target_size, source + i - (series_counter), series_counter);
		target_size += series_counter;
	}
}

// Do buffered RLE decoding
void Componentality::Common::RLE_Decode(unsigned char* source, size_t source_size, unsigned char* target, size_t& target_size)
{
	target_size = 0;
	for (size_t i = 0; i < source_size;)
	{
		unsigned char size = source[i] & ~0x80;
		if (source[i] & 0x80)
		{
			memset(target + target_size, source[i + 1], size);
			i += 2;
		}
		else
		{
			memcpy(target + target_size, source + i + 1, size);
			i += size + 1;
		}
		target_size += size;
	}
}

// Check where two buffers are different
size_t Componentality::Common::isDiff(unsigned char* left, unsigned char* right, size_t size)
{
	for (size_t i = 0; i < size; i++)
	{
		if (left[i] != right[i])
			return i;
	}
	return (size_t)-1;
}

// Incremental decoding initialization
void Componentality::Common::RLE_InitDecoder(RLE_DECODE* handler, unsigned char* source)
{
	handler->mDecodedPortion = 0;
	handler->mPtr = 0;
	handler->mOffset = 0;
	handler->mSource = source;
}

// Decode next byte incrementally
unsigned char Componentality::Common::RLE_Fetch(RLE_DECODE* handler)
{
	if (handler->mDecodedPortion > handler->mPtr)
	{
		handler->mPtr += 1;
		if (handler->mMode == 0x00)
			handler->mOffset += 1;
		return handler->mSource[handler->mOffset - 1];
	}
	handler->mDecodedPortion = handler->mSource[handler->mOffset] & ~0x80;
	handler->mMode = handler->mSource[handler->mOffset] & 0x80;
	handler->mOffset += 2;
	handler->mPtr = 1;
	return handler->mSource[handler->mOffset - 1];
}
