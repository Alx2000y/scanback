/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* Scanback compression implementation                                      */
/****************************************************************************/
#ifndef __SCANBACK_H__
#define __SCANBACK_H__

// Width of scanning back window (in bits)
#define WINDOW_BITS 7
// Length of repeating sequence to be encoded
#define LENGTH_BITS 4
// Number of bits for length of sequence of non-repeating items
#define NON_REPEATING_SIZE_BITS 4

namespace Componentality
{
	namespace Common
	{
		// Backward scanned sequence descriptor
		typedef struct
		{
			unsigned char offset;
			unsigned char length;
		} SB_PTR;

		// Decoder context
		typedef struct
		{
			unsigned long bitindex;							// Bit pointer in encoded data memory
			unsigned char* mem;								// Encoded data memory
			unsigned long total_decoded;					// Totally decoded input bytes
			unsigned long index;							// Number of decoded bytes sent to output
			unsigned char decoded_buf[1 << WINDOW_BITS];	// Storage buffer for decoded bytes (vocabulary)
			void*    brb;									// Incoming data ring buffer (used if not NULL), shall be casted to BIT_RINGBUF*
		} SB_DECODER;

		// Compress buffer <buf> of size <size> to bitwise memory <mem> and return number of bits produced
		unsigned long SB_Compress(unsigned char* mem, unsigned char* buf, unsigned long size);
		// Fill decoder context with initial values. <mem> is source buffer for decoding
		void SB_InitDecoder(SB_DECODER* decoder, unsigned char* mem);
		// Initialize decoder with ringbuffer
		void SB_InitDecoderRB(SB_DECODER* decoder, void* ringbuffer);
		// Decode single byte from input stream
		unsigned char SB_Fetch(SB_DECODER* decoder);

	} // namespace Common
} // namespace Componentality

#endif