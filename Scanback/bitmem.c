/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* Bit oriented operations                                                  */
/****************************************************************************/

#include "bitmem.h"

// Get single bit from bit array
bit bitmem_get1(unsigned char* mem, unsigned int index)
{
	return ((mem[index / 8] >> (index % 8)) & 0x01) == 1;
}

// Put single bit to the bit array
void bitmem_put1(unsigned char* mem, unsigned int index, bit bitval)
{
	if (bitval)
		mem[index / 8] |= 0x01 << (index % 8);
	else
		mem[index / 8] &= ~(0x01 << (index % 8));
}

// Retrieve given number of bits from the bits array
unsigned long long bitmem_get(unsigned char* mem, unsigned int index, unsigned char numbits)
{
	register unsigned long long result = 0;
	unsigned char i;
	for (i = 0; i < numbits; i++)
	{
		result |= bitmem_get1(mem, index++) << i;
	}
	return result;
}

// Put given number of bits to the bits array
void bitmem_put(unsigned char* mem, unsigned int index, unsigned long long val, unsigned char numbits)
{
	for (; numbits > 0; numbits--)
	{
		bitmem_put1(mem, index++, val & 0x01);
		val >>= 1;
	}
}

/////////////////////////////////////////////////////////////////////////

// Initialize ring buffer
void bitmem_init_ringbuf(BIT_RINGBUF* brb, 
	unsigned char* mem, 
	unsigned long memsize_bytes, 
	BIT_CALLBACK overflow_cb, 
	BIT_CALLBACK underflow_cb)
{
	brb->memory = mem;
	brb->size = memsize_bytes * 8;
	brb->free = 0;
	brb->bit_read_ptr = 0;
	brb->bit_write_ptr = 0;
	brb->overflow_cb = overflow_cb;
	brb->underflow_cb = underflow_cb;
}

// Write bits to the ring buffer
void bitmem_write_ringbuf(BIT_RINGBUF* brb, unsigned long long val, unsigned char numbits)
{
	while (numbits > brb->free)
		brb->overflow_cb(brb);
	unsigned long remain = brb->size - brb->bit_write_ptr;
	if (remain > numbits)
	{
		bitmem_put(brb->memory, brb->bit_write_ptr, val, numbits);
		brb->bit_write_ptr += numbits;
	}
	else
	{
		bitmem_put(brb->memory, brb->bit_write_ptr, val, (unsigned char) remain);
		brb->bit_write_ptr = 0;
		val >>= remain;
		bitmem_put(brb->memory, brb->bit_write_ptr, val, (unsigned char) ((unsigned long)numbits - remain));
		brb->bit_write_ptr = numbits - remain;
	}
	brb->free -= numbits;
}

// Retrieve given number of bits from the ring buffer
unsigned long long bitmem_read_ringbuf(BIT_RINGBUF* brb, unsigned char numbits)
{
	unsigned long long result;
	while (brb->size - brb->free < numbits)
		brb->underflow_cb(brb);
	unsigned long remain = brb->size - brb->bit_read_ptr;
	if (remain > numbits)
	{
		result = bitmem_get(brb->memory, brb->bit_read_ptr, numbits);
		brb->bit_read_ptr += numbits;
	}
	else
	{
		result = bitmem_get(brb->memory, brb->bit_read_ptr, (unsigned char) remain);
		brb->bit_read_ptr = 0;
		result |= bitmem_get(brb->memory, brb->bit_read_ptr, (unsigned char) ((unsigned long) numbits - remain)) << remain;
		brb->bit_read_ptr = numbits - remain;
	}
	brb->free += numbits;
	return result;
}
