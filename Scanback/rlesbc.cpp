/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* Combined RLE + Scanback implementation (compression is to be done        */
/* sequentially, decompression is optimized                                 */
/****************************************************************************/
#include "rlesbc.h"

using namespace Componentality::Common;

// Decode next byte incrementally for stream compressed by both RLE and Scanback
unsigned char Componentality::Common::RLESB_Fetch(RLE_DECODE* handler, SB_DECODER* sb_handler, unsigned char* repeating_value)
{
	if (handler->mDecodedPortion > handler->mPtr)
	{
		handler->mPtr += 1;
		if (handler->mMode == 0x00)
			*repeating_value = SB_Fetch(sb_handler);
		return *repeating_value;
	}
	*repeating_value = SB_Fetch(sb_handler);
	handler->mDecodedPortion = *repeating_value & ~0x80;
	handler->mMode = *repeating_value & 0x80;
	*repeating_value = SB_Fetch(sb_handler);
	handler->mPtr = 1;
	return *repeating_value;
}
